/*
Example :
 @viewport
 class name  extends Component{
    render(){
        const {innerWidth,innerHeight } = this.props.viewport
        ...
    }
 }


 */

const React = require('react');
// https://github.com/JedWatson/exenv


export default function myDecorator(paramsForDecorator) {
    function InjectFunction(ComposedComponent) {
        return class Viewport extends React.Component {
            constructor() {
                super();
                this.sendTimer = 0;
                this.setTimeoutTimer = 0;
                this.viewport = { innerWidth: window.innerWidth, innerHeight: window.innerHeight }
                this.delay = typeof paramsForDecorator === "function" ? 0 : paramsForDecorator
                this.state = {
                    viewport: this.viewport
                };
            }
            handleWindowResize = () => {
                if (this.viewport.innerWidth !== window.innerWidth || this.viewport.innerHeight !== window.innerHeight) {
                    this.viewport = { innerWidth: window.innerWidth, innerHeight: window.innerHeight };
                    this.handleResize(this.viewport)
                }
            }
            componentDidMount() {
                window.addEventListener('resize', this.handleWindowResize);
                window.addEventListener('orientationchange', this.handleWindowResize);
                setTimeout(this.handleWindowResize.bind(this),3000,true);
                setTimeout(this.handleWindowResize.bind(this),1000,true);
                this.handleResize({ innerWidth: 0, innerHeight: 0})
                this.viewport.innerWidth = 0;
                this.viewport.innerHeight = 0;
                this.handleWindowResize();
            }

            componentWillUnmount() {
                this.isUnmounted = true
                window.removeEventListener('resize', this.handleWindowResize);
                window.removeEventListener('orientationchange', this.handleWindowResize);
            }

            render() {
                return <ComposedComponent {...this.props} viewport={this.state.viewport}/>;
            }

            handleResize(value) {
                if(this.delay == 0 || this.sendTimer > 10) {
                    this.sendTimer = 0;
                    this.handleResizeDelay(value)
                }else{
                    this.sendTimer++;
                    clearTimeout(this.setTimeoutTimer)
                    this.setTimeoutTimer = setTimeout(this.handleResizeDelay , this.delay , value )
                }
            }
            handleResizeDelay = (value) => {
                if(this.isUnmounted){
                    return
                }
                this.setState({viewport: value});
            }

        };
    }
    if(typeof paramsForDecorator === "function"){
        return InjectFunction(paramsForDecorator);
    }else{
        return InjectFunction
    }
}