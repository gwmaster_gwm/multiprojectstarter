// http ajax
import axios from 'axios';

export const LOADING = "LOADING"
export const ERROR = "ERROR"


export function* get(url , defualtResponsOnError = ERROR) {
    try{
        const request = yield axios.get(url);
        return request.data
    }catch(e){
        console.debug("faild "+url)
        console.log(e)
        return defualtResponsOnError;
    }
}


export function* post(url , data, defualtResponsOnError = ERROR) {
    try{
        return yield axios.post(url,data);
    }catch(e){
        console.debug("faild "+url)
        console.log(e)
        return defualtResponsOnError
    }
}