export default (actionName,defaultPayload = null) => (state = null , action) => {
    if(action.type == actionName){
        if( action.payload == undefined){
            console.error(actionName+" payload is undefiend ",action)
        }
        return action.payload;
    }
    if(state == null){
        return defaultPayload;
    }
    return state;
}