import React from 'react'
import { Route, Switch } from 'react-router'
import NavBar from '../containers/navBar/NavBar'
import AntThemeExample from '../components/AntTheme/AntThemeExample'
import I18 from '../containers/i18Example/i18Example'
import Saga from '../containers/myComponent/myComponent'
import Login from '../containers/RoutesExample/exampleLogin'

// https://github.com/ReactTraining/react-router/issues/5053

const routes = (
    <div>
        <NavBar/>
        <Switch>
            <Route exact path="/" component={AntThemeExample} />
            <Route exact path="/i18" component={I18} />
            <Route exact path="/saga" component={Saga} />
            <Route path="/loginExample" component={Login}/>
        </Switch>
    </div>
)

export default routes