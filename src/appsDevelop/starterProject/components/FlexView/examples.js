import React , {Component} from 'react';
import FlexView from 'react-flexview'
import {Box,Box2} from '../ColorBox'

export default class  extends Component{
    render(){
        return(
            <div>
                <br/>
                FlexView 1
                <FlexView id='app' width='100%' height='100%'>
                    <FlexView id='left-sidebar' basis={200} ><Box/></FlexView>
                    <FlexView id='content' grow ><Box/></FlexView>
                    <FlexView id='right-sidebar' basis={200} ><Box/></FlexView>
                </FlexView>
                FlexView 2:
                <FlexView column height={120} hAlignContent='center' vAlignContent='center'>
                   <Box2/>
                   <Box2/>
                </FlexView>
            </div>
        )
    }
}