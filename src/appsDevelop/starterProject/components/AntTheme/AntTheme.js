import React, { Component, Fragment } from "react";
import {
    Row,
    Col,
    message,
    Button,
    Drawer,
    Switch
} from "antd";

import ColorPicker from "../ColorPicker";

// Ant Theme
import '../../styles/theme.less'
// import  initialValue from '../../styles/themeVariables'

// created in webPack webpack.DefinePlugin
const __ANT_THEME_VARS__ = process.env.__ANT_THEME_VARS__;


function objToArr(obj) {
    let themeVariables = {}
    function deepLoop(obj) {
        for(let i in obj) {
            if(typeof obj[i] === 'object'){
                deepLoop(obj[i]);
            }else{
                themeVariables[i] = obj[i]
            }
        }
    }
    deepLoop(obj)
    return themeVariables;
}


export default class App extends Component {
    constructor(props) {
        super(props);
        let vars = {};
        let initialValue =  __ANT_THEME_VARS__;
        try {
            vars = Object.assign({}, initialValue, JSON.parse(localStorage.getItem('app-theme')));
        } finally {
            this.state = {
                isLeft : false,
                visible : true,
                vars,
                initialValue,
                themeColorPreset : this.getThemeColors(vars)
            };
            window.less
                .modifyVars(objToArr(vars))
                .then(() => { })
                .catch(error => {
                    message.error(`Failed to update theme`);
                });
        }
    }

    getThemeColors = (vars) => {
        return [...new Set(Object.values(objToArr(vars)))]
    }

    showDrawer = () => {
        this.setState({
            visible: !this.state.visible,
        });
    };

    onClose = () => {
        const { vars } = this.state;
        this.setState({
            themeColorPreset :  this.getThemeColors(vars),
            visible: false,
        });
    };

    onChangeComplete = (varName, color) => {
        const { vars } = this.state;
        vars[varName] = color;
        this.setState({vars});
    };

    handleClose = (varName , path) => () => {
        let newColor = path == null ?  this.state.vars[varName] : this.state.vars[path][varName]
        const { vars } = this.state;
        this.setState({
            themeColorPreset : [...new Set([ newColor , ...this.state.themeColorPreset])]
        });
    }
    handleColorChange = (varname, color ,path) => {
        let vars = {...this.state.vars};
        if(path != null){
            vars[path][varname] = color;
        }else{
            vars[varname] = color;
        }
        window.less
            .modifyVars(objToArr(vars))
            .then(() => {
                this.setState({ vars });
                localStorage.setItem("app-theme", JSON.stringify(vars));
            })
            .catch(error => {
                message.error(`Failed to update theme`);
            });
    };
    resetTheme = () => {
        console.log(this.state.initialValue )
        localStorage.setItem('app-theme', '{}');
        this.setState({ vars: {...this.state.initialValue }});
        window.less
            .modifyVars(objToArr(this.state.initialValue))
            .catch(error => {
                message.error(`Failed to reset theme`);
            });
    }
    getColorPicker = (varName , path = null) => (
        <Row key={varName}>
            <Col span={4}>
                <ColorPicker
                    type="sketch"
                    small
                    color={path == null ?  this.state.vars[varName] : this.state.vars[path][varName]}
                    position="bottom"
                    presetColors={this.state.themeColorPreset}
                    onChangeComplete={color => this.handleColorChange(varName, color , path)}
                    handleClose={this.handleClose(varName,path)}
                />
            </Col>
            <Col span={20}>{varName}</Col>
        </Row>
    )

    onChange = (checked) => {
        this.setState({
            isLeft : checked
        })
    }

    drawerTitile = () => {
        return <Row>
                    <Col span={18}>
                        Ant theme manager
                    </Col>
                    <Col span={6}>
                        <Switch checkedChildren="Left" unCheckedChildren="Right" onChange={this.onChange} checked={this.state.isLeft} />
                    </Col>
                </Row>
    }
    fieldChange = (varName) => () => {
        this.handleColorChange(varName,"50px");
    }


    colorPickers = () => {
        //const colorPickers = Object.keys(this.state.vars).map(varName => this.getValueHandler(varName));
        let items = []
        const { vars } = this.state;
        for(let i in vars){
            if(typeof vars[i] === 'object'){
                items.push(<div style={{textAlign: 'center' ,  padding: '10px'}}>{i}</div>)
                let colorPickers = Object.keys(vars[i]).map(varName => this.getColorPicker(varName , i));
                items = [...items , ...colorPickers]
            }else{
                items = [...items , this.getColorPicker(i)]
            }
        }
        return <Fragment> {items} </Fragment>
    }

    render(){


        return(
            <div>
                <Button type="primary" onClick={this.showDrawer}>
                    Open Theme Manager
                </Button>
                <Drawer
                    title={this.drawerTitile()}
                    placement={this.state.isLeft ? "left" : "right"}
                    mask={false}
                    closable={false}
                    onClose={this.onClose}
                    visible={this.state.visible}
                    width={270}
                >
                    {this.colorPickers()}

                    <div
                        style={{
                            position: 'absolute',
                            bottom: 0,
                            width: '100%',
                            borderTop: '1px solid #e8e8e8',
                            padding: '10px 16px',
                            textAlign: 'right',
                            left: 0,
                            background: '#fff',
                            borderRadius: '0 0 4px 4px',
                        }}
                    >
                        <Row style={{
                            marginRight: 8,
                        }}>
                            <Col span={14}>
                                <Button type="primary" onClick={this.resetTheme}>
                                    Reset Theme
                                </Button>
                            </Col>
                            <Col span={10}>
                                <Button onClick={this.onClose}>
                                    Close
                                </Button>
                            </Col>
                        </Row>
                    </div>
                </Drawer>
            </div>
        )
    }
}
