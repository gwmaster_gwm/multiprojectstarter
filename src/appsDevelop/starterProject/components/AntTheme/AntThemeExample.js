import React , {Component} from 'react';
import {
    Button,
    Card
} from "antd";

export default class  extends Component{
    render(){
        return(
            <div>
                <div className="myColor" >
                    @my-color
                </div>
                <Button size='small'>btn-default-bg small</Button>
                <Card title="Card title" extra={<a href="#">More</a>} style={{ width: 300 }}>
                    <p>Card content</p>
                    <p>Card content</p>
                    <p>Card content</p>
                </Card>
            </div>

        )
    }
}