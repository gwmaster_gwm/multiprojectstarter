import React , {Component} from 'react';
import _ from 'lodash'
import { Grid, Cell } from "styled-css-grid";
import {Box,Box2} from '../ColorBox'

const rows = counts =>
    _.flatMap(counts, number =>
        _.range(number).map(i =>
            <Cell width={12 / number} key={`${number}_${i}`}>
                <Box>
                    {i + 1}/{number}
                </Box>
            </Cell>
        )
    );


export default class  extends Component{
    render(){
        return(
            <div>
                CSS HTML5 Grid React
                <Grid columns={12} minRowHeight="45px">
                    {rows([12, 6, 4, 2, 1])}
                </Grid>
            </div>
        )
    }
}