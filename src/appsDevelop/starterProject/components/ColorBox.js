import React , {Component} from 'react';

export function getRandomColor() {
 return "hsl(" + Math.random() * 360 + ", 100%, 75%)";
}

export class Box extends Component{
    render(){
        let s = {
            background: getRandomColor(),
            width: '100%',
            height : '30px'
        }
        return <div style={s}>{this.props.children ? this.props.children : null}</div>
    }
}

// React Stateless Functional Components
// https://hackernoon.com/react-stateless-functional-components-nine-wins-you-might-have-overlooked-997b0d933dbc
export const Box2 = () => {
    let s = {
        background: getRandomColor(),
        height : '30px',
        width: '30px'
    }
    return <div style={s}></div>
}