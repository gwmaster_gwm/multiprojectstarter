const LOCAL_IP = 'http://localhost:3010'

let ROOT_URL = ""
let isDebug = false;

if (location.hostname === "localhost"){
    // localhost debug server
    ROOT_URL =  LOCAL_IP
    isDebug = true;
}

export const MAIN_CONFIG = {
    ROOT_URL,
    isDebug,
};

export const version = "v0.1"