import {globalReducer} from '../framwroks'

export function checkBindAction(...actionsList) {
    let bindActions = {}
    let error = false;
    for (let actions in actionsList) {
        for(let fn in actionsList[actions]){
            if( bindActions[fn] != undefined){
                alert(`Develop bug same action name ${fn}`)
                console.error(`Same action name ${fn}`)
            }
            bindActions[fn] = actionsList[actions][fn]
        }
    }
    return bindActions
}



export function autoReducer(...reducersList) {
    let reducers = {}
    for (let red in reducersList) {
        for(let varName in reducersList[red]) {
            if( reducers[varName] != undefined){
                alert("store have var this name "+varName)
            }
            reducers[varName] = reducersList[red][varName];
            if(Array.isArray(reducers[varName])){
                // use defualt reducer
                let arr = reducers[varName]
                reducers[varName] = globalReducer(arr[0],arr[1])
            }
        }
    }
    return reducers

}