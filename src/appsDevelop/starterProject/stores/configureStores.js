import { createStore, combineReducers , compose ,  applyMiddleware } from 'redux';
// init promise
// pause action and wait recive respons from proomse and then pas it to reducers
// on payload on event
import ReduxProomse from 'redux-promise'
import createSagaMiddleware from 'redux-saga';

import rootSaga from './sagas';
import reducers from './reducers';

// https://github.com/elgerlambert/redux-localstorage
// https://github.com/elgerlambert/redux-localstorage/tree/1.0-breaking-changes
import persistState from 'redux-localstorage'

// load Translations
import thunk from 'redux-thunk';
import {syncTranslationWithStore, i18nReducer } from 'react-redux-i18n';

//Router v4
import {
    //createBrowserHistory as router,
    createHashHistory as router
} from 'history'
import { routerMiddleware, connectRouter } from 'connected-react-router'

// https://medium.com/@pshrmn/a-simple-react-router-v4-tutorial-7f23ff27adf
// https://github.com/ReactTraining/react-router/issues/5053
//const history = createBrowserHistory()
const history = router();


const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const sagaMiddleware = createSagaMiddleware();

let combinedReducers =   combineReducers({
    ...reducers,
    i18n: i18nReducer,
})

const store = createStore(
    connectRouter(history)(combinedReducers),
    composeEnhancer(
        persistState(/*paths, config*/),
        applyMiddleware(
            sagaMiddleware ,
            ReduxProomse ,
            thunk,
            routerMiddleware(history)
        )
    )
)

// dispatch Translations
syncTranslationWithStore(store)

const configureStore = () => {
    return {
        ...store,
        history,
        runSaga: sagaMiddleware.run(rootSaga)
    }
}


export default configureStore;