import { fork } from 'redux-saga/effects';

import NavBar from '../containers/navBar/NavBar.sagas'
import myComponent from '../containers/myComponent/myComponent.sagas'
import i18 from '../containers/i18Example/i18Example.sagas'

export default function* sagaMiddleware() {
    yield fork(NavBar)
    yield fork(myComponent)
    yield fork(i18)
}