const initialValue = {
    '@top-color': '#ff3217',
    '@body-background' : '#dbcbff',
    "Buttons" : {
        '@btn-primary-bg': '#397dcc',
    },
    "My Colors" : {
        '@my-color': '#ff3217'
    }
}

module.exports = initialValue