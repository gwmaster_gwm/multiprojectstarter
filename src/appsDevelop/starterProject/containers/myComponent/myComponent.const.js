const pr = "MY_COMPONENTS_"

export const MY_COMPONENTS = {
    ADD : `${pr}ADD`
}

export const MY_COMPONENTS_SAGA = {
    ADD : `${pr}ADD_SAGA`
}