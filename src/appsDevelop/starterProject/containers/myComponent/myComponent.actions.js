import {MY_COMPONENTS_SAGA}  from './myComponent.const'


function setData(data) {
    return{
        type : MY_COMPONENTS_SAGA.ADD,
        payload: data
    }
}

function removeData() {
    return{
        type : MY_COMPONENTS_SAGA.ADD,
        payload: ''
    }
}

function addEvent() {
    return{
        type : MY_COMPONENTS_SAGA.ADD,
        payload: ''
    }
}


export const myComponentActions = {
    setData,
    removeData,
    addEvent
}





