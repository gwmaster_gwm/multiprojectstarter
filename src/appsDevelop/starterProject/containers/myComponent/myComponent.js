import React , {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {checkBindAction} from '../../stores/actions'
import {myComponentActions } from './myComponent.actions'
import {moreActions} from './MoreActions'
import {MY_COMPONENTS_SAGA} from  './myComponent.const'

class TestContainer extends Component{
    setData = () => {
        this.props.setData({date  : new Date().getTime() , txt : "simple text"})
    }
    render(){
        return(
           <div>
               Redux Saga example:
               <br/>
               <button onClick={this.setData}>  {MY_COMPONENTS_SAGA.ADD}</button>
               <button onClick={this.props.removeData}> Remove</button>
               <button onClick={this.props.newEeven}>newEeven</button>
               <br/>
               {this.props.myComponent_StoreVar}
               <br/>
               Time : {this.props.myComponent_time}
           </div>
        )
    }

}

function mapActionsToProps(dispatch) {
    return bindActionCreators(
        {
            ...checkBindAction(myComponentActions),
            // ...checkBindAction(myComponentActions , moreActions), - example thet will throw warning use same action name
            //  ...myComponentActions - export all events
            newEeven : moreActions.newEeven // add specific event from problematic package
        }
        ,dispatch)
}

function mapStoreToProps({myComponent_StoreVar , myComponent_time} , ownProps) {
    return  {
        myComponent_StoreVar,
        myComponent_time
    }
}

export default connect(mapStoreToProps,mapActionsToProps)(TestContainer)