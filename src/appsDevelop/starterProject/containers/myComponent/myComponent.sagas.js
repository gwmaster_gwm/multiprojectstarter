import { all , takeLatest , select , put , call} from 'redux-saga/effects'
//import {MY_COMPONENTS,MY_COMPONENTS_SAGA} from './myComponent.const'
import {MY_COMPONENTS_SAGA , MY_COMPONENTS} from '../../stores/constants'

export const delay = (ms) => new Promise(res => setTimeout(res, ms))

function* addSaga({payload : {date , txt}}) {
    let {
        myComponent_time
    }= yield select(state => state)


    let t = date+" "+txt
    t += "_SAGA myComponent_time:"+myComponent_time
    // freez saga
    yield delay(myComponent_time)

    yield all([
        put({type: MY_COMPONENTS.ADD, payload: t}),
    ])

    yield call(runOtherSaga)

}

function* runOtherSaga() {
    let {
        myComponent_StoreVar
    }= yield select(state => state)

    console.log("show old value "+myComponent_StoreVar)
}

function* listenToReducerChange(){
    let {
        myComponent_StoreVar
    }= yield select(state => state)

    console.log(myComponent_StoreVar)
}


export default function* () {
    yield takeLatest(MY_COMPONENTS_SAGA.ADD, addSaga);
    yield takeLatest( MY_COMPONENTS.ADD, listenToReducerChange);

}