import React, {Component} from 'react';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux';
import {checkBindAction} from '../../stores/actions'
import {Translate, Localize} from 'react-redux-i18n'
import {i18Actions} from './i18Example.actions'
import FlexView from 'react-flexview'

import {Button} from 'antd'

export class i18Example extends Component {
    setLanguage = (ln) => () => {
        this.props.setLanguage(ln)
    }

    render() {
        return (
            <div>
                Translate i18n example
                <br/>
                <Button   type="primary" onClick={this.props.loadTranlsate}>load translate</Button>
                <Button   type="primary" onClick={this.setLanguage('en')}>English</Button>
                <Button   type="primary" onClick={this.setLanguage('nl')}>Netherlands</Button>
                <FlexView column  vAlignContent='center'>
                    <Translate value="application.title"/>
                    {/*// => returns '<span>Toffe app met i18n!</span>' for locale 'nl'*/}

                    <Translate value="application.hello" name="Aad" moreText={"runtime text"}/>
                    {/*// => returns '<span>Hallo, Aad!</span>' for locale 'nl'*/}

                    <Localize value="2015-09-03" dateFormat="date.long"/>
                    {/*// => returns '<span>3 september 2015</span> for locale 'nl'*/}
                    {/*<Localize value={10/3} options={{style: 'currency', currency: 'EUR', minimumFractionDigits: 2, maximumFractionDigits: 2}}/>*/}
                    {/*// => returns '<span>€ 3,33</span> for locale 'nl'*/}

                    <Translate value="export" count={1}/>
                    {/*// => returns '<span>Exporteer 1 ding</span> for locale 'nl'*/}

                    <Translate value="export" count={2}/>
                    {/*// => returns '<span>Exporteer 2 dingen</span> for locale 'nl'*/}

                    <Translate value="two_lines" dangerousHTML/>
                    {/*// => returns '<span>Regel 1<br />Regel 2</span>'*/}
                </FlexView>
            </div>

        )
    }
}

function mapActionsToProps(dispatch) {
    return bindActionCreators(
        {
            ...checkBindAction(i18Actions)
        }
        , dispatch)
}

function mapStoreToProps(state, ownProps) {
    return {}
}

export default connect(mapStoreToProps, mapActionsToProps)(i18Example)