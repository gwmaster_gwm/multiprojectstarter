import {I18}  from './i18Example.const'


function loadTranlsate() {
    return{
        type : I18.LOAD_TRANSLATE_SAGA
    }
}

function setLanguage(ln) {
    return{
        type : I18.SET_LANGUAGE,
        payload: ln
    }
}




export const i18Actions = {
    loadTranlsate,
    setLanguage
}





