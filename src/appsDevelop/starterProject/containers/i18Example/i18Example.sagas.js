import { all , takeLatest , select , put} from 'redux-saga/effects'

import { loadTranslations, setLocale} from 'react-redux-i18n';
import {I18} from './i18Example.const'

function* loadTranslate() {
    const translationsObject = {
        en: {
            application: {
                title: 'Awesome app with i18n!',
                hello: 'Hello, %{name}!  %{moreText}'
            },
            date: {
                long: 'MMMM Do, YYYY'
            },
            export: 'Export %{count} items',
            export_0: 'Nothing to export',
            export_1: 'Export %{count} item',
            two_lines: 'Line 1<br />Line 2',
            literal_two_lines: 'Line 1\
Line 2'
        },
        nl: {
            application: {
                title: 'Toffe app met i18n!',
                hello: 'Hallo, %{name}!'
            },
            date: {
                long: 'D MMMM YYYY'
            },
            export: 'Exporteer %{count} dingen',
            export_0: 'Niks te exporteren',
            export_1: 'Exporteer %{count} ding',
            two_lines: 'Regel 1<br />Regel 2',
            literal_two_lines: 'Regel 1\
Regel 2'
        }
    };


    yield all([
        put(loadTranslations(translationsObject)),
        put(setLocale('en'))
    ])
}

function* setLanguage({payload}){
    yield all([
        put(setLocale(payload))
    ])
}

export default function* () {
    yield takeLatest(I18.LOAD_TRANSLATE_SAGA, loadTranslate);
    yield takeLatest(I18.SET_LANGUAGE,setLanguage)
}