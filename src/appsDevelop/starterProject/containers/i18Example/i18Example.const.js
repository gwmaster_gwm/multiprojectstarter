const p = "i18_"

export const I18 = {
    LOAD_TRANSLATE_SAGA : `${p}LOAD_TRANSLATE_SAGA`,
    SET_LANGUAGE  : `${p}SET_LANGUAGE`
}