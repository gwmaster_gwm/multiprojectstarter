import React , {Fragment , Component} from 'react'
import {bindActionCreators} from 'redux';
import { connect  } from 'react-redux'
import { Link } from 'react-router-dom'
import AntTheme from '../../components/AntTheme/AntTheme'
import {checkBindAction} from '../../stores/actions'
import {NavBarActions} from './NavBar.action'

import { Tabs } from 'antd';
const TabPane = Tabs.TabPane;


const operations =  <AntTheme/>;

class NavBar extends Component{
    tabCallback = (key) => {
        this.props.navigate(key)
    }
    render(){
        return(
            <Fragment>
                <Tabs activeKey={this.props.pathname} onChange={this.tabCallback} type="card" tabBarExtraContent={operations}>
                    <TabPane tab="Ant Theme Example" key="/"></TabPane>
                    <TabPane tab="i18" key="/i18"></TabPane>
                    <TabPane tab="Saga Example" key="/saga"></TabPane>
                    <TabPane tab="Login Example" key="/loginExample"></TabPane>
                </Tabs>
                <a  target="_blank" href='https://bitbucket.org/gwmaster_gwm/multiprojectstarter/src/master/'>https://bitbucket.org/gwmaster_gwm/multiprojectstarter/src/master/</a>
                <br/>
                <div><Link to="/">Ant Theme Example</Link> <Link to="/i18">i18</Link> <Link to="/saga">Saga Example</Link></div>

            </Fragment>
        )
    }
}


const mapStateToProps = state => ({
    pathname: state.router.location.pathname,
    search: state.router.location.search,
    hash: state.router.location.hash,
})


function mapActionsToProps(dispatch) {
    return bindActionCreators(
        {
            ...checkBindAction(NavBarActions)
        }
        ,dispatch)
}

export default connect(mapStateToProps , mapActionsToProps)(NavBar)