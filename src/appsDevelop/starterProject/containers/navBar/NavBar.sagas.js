import { push } from 'connected-react-router'
import { all , takeLatest , select , put , call} from 'redux-saga/effects'
import {NAVIGATION} from './NavBar.const'

function* navigate({payload}) {
    yield put(push(payload))
}

export default function* () {
    yield takeLatest(NAVIGATION.NAVIGATE_SAGA, navigate);

}