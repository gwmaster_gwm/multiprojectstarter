import {NAVIGATION} from './NavBar.const'

function navigate(path) {
    return{
        type : NAVIGATION.NAVIGATE_SAGA,
        payload: path
    }
}
export const NavBarActions = {
    navigate
}





