import React , {Fragment} from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router'

import {ErrorBoundary} from '../../framwork'
import configureStore from './stores/configureStores';
const store = configureStore();
import routes from './routes'

ReactDOM.render(
    <ErrorBoundary>
        <Provider store={store}>
            <ConnectedRouter history={store.history}>
                {routes}
            </ConnectedRouter>
        </Provider>
    </ErrorBoundary>
    ,  document.getElementById("app"));
