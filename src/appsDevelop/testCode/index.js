import React , {Component} from 'react';
import ReactDom from 'react-dom'

import _ from 'lodash'
// var webpack = require('webpack');
const initialValue = {
    '@top-color': '#ff3217',
    "Buttons" : {
        '@btn-primary-bg': '#397dcc',
    },
    "DeepCategory" : {
        "MyVars": {
            '@my-color': '#ff3217'
        }
    }
}


let arr = []
function deepLoop(obj) {
    for(let i in obj) {
        if(typeof obj[i] === 'object'){
            deepLoop(obj[i]);
        }else{
            arr.push(i)
        }
    }
}
deepLoop(initialValue)



class Develop  extends Component{
    render(){
        return(
            <div>
                Test Code
                <br/>
                {JSON.stringify(arr)}
            </div>
        )
    }
}


ReactDom.render( <Develop/> ,  document.getElementById("app"));