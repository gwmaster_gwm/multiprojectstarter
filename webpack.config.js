const HtmlWebPackPlugin = require("html-webpack-plugin");
const AntDesignThemePlugin = require('antd-theme-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var webpack = require('webpack')
var fs = require('fs');
const lessToJs = require('less-vars-to-js');

const path = require('path');

const prod = process.argv.indexOf('-p') !== -1;
var isDevelopment = process.argv.indexOf('development') !== -1

var projectPath = "demoProject" // defualt
var appName = projectPath;

//  https://github.com/mzohaibqc/antd-theme-webpack-plugin

try {
    projectPath = process.argv[ process.argv.length-1].split("=")[1];
}catch(e){
    console.log("defualt project : "+projectPath)
}


// split to folder and appName
var appFolder = "apps/"
if(projectPath.split("/").length > 1){
    var p = projectPath.split("/");
    appName = p.pop();
    appFolder = p.join("/")
}

console.log("projectPath : "+projectPath)
console.log("appName : "+appName)
console.log("folder : "+appFolder)


module.exports = {
    module: {
        rules: [
            {
                type: 'javascript/auto',
                test: /\.mjs$/,
                use: []
            },
            {
                enforce: "pre",
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
                options: {
                    emitError: true,
                    quiet: false, // Loader will process and report errors only and ignore warnings if this option is set to true
                }
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: "html-loader",
                        options: { minimize: true }
                    }
                ]
            },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader!less-loader?noIeCompat"
            },
            {
                test: /.(png|gif|jpg|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/,
                loader: 'url-loader?limit=100000'
            },
            // https://github.com/bholloway/resolve-url-loader fix SCSS problem
            {
                test   : /\.css$/,
                loaders: ['style-loader', 'css-loader?url=false']
            }, {
                test   : /\.scss$/,
                loaders: ['style-loader', 'css-loader?url=false' , 'sass-loader?sourceMap']
            },
        ]
    },
    entry : './src/'+appFolder+'/'+appName+'/index.js',
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/'+appFolder+'/'+appName+"/index.html",
            filename: "./index.html"
        }),
        new CopyWebpackPlugin([ { from: 'src/assets', to: 'assets' } ]), // copy global assets to project
        new CopyWebpackPlugin([ { from: './src/'+appFolder+'/'+appName+'/assets', to: 'assets' } ]), // copy project assets to project
        new webpack.DefinePlugin({
            __VERSION__: JSON.stringify('12345')
        })
    ]
};

console.log("isDevelopment : "+isDevelopment)



var dist = "/dist"
if(prod){
    dist = "/dist-pro"
}


if(!isDevelopment) {
    console.log("build to "+dist+"/"+appName)
    var randomn
    module.exports.output =  {
        path: __dirname + dist+'/'+appName,
        publicPath: '.',
        filename: "index.js"
    }
    module.exports.plugins =  [
        new HtmlWebPackPlugin({
            template: './src/'+appFolder+'/'+appName+"/index.html",
            filename: "./index.html",
            hash : true
        }),
        new CopyWebpackPlugin([{from: 'src/assets', to: './assets'}]), // copy assets to project
        new CopyWebpackPlugin([ { from: './src/'+appFolder+'/'+appName+'/assets', to: './assets' } ]), // copy project assets to projec
        new webpack.DefinePlugin({
            __VERSION__: JSON.stringify(new Date().getTime()) // accsesble from react by __VERSION__
        })
    ]

}

// Ant Theme
var  variables_less = './src' + projectPath + '/styles/variables.less';
if (fs.existsSync(variables_less)) {
    console.log("Ant Theme active : true")

    // read less file and create js
    var initialValue = lessToJs(fs.readFileSync(path.join(__dirname, variables_less), 'utf8'));
    var themeVariablesFile = './src' + projectPath + '/styles/themeVariables.js'
    if (fs.existsSync(themeVariablesFile)) {
        initialValue = require(themeVariablesFile);
    }
    let themeVariables = []
    function deepLoop(obj) {
        for(let i in obj) {
            if(typeof obj[i] === 'object'){
                deepLoop(obj[i]);
            }else{
                themeVariables.push(i)
            }
        }
    }
    deepLoop(initialValue)
    console.log("initialValue")
    console.log(initialValue)
    console.log("themeVariables")
    console.log(themeVariables)

    const options = {
        antDir: path.join(__dirname, './node_modules/antd'),
        stylesDir: path.join(__dirname, './src' + projectPath + '/styles'),
        varFile: path.join(__dirname, './src' + projectPath + '/styles/variables.less'),
        mainLessFile: path.join(__dirname, './src' + projectPath + '/styles/index.less'),
        themeVariables: themeVariables,
        indexFileName: false
    }

    const themePlugin = new AntDesignThemePlugin(options);
    module.exports.plugins.push(themePlugin)
    module.exports.plugins.push(
    new webpack.DefinePlugin({
        'process.env': {
            __ANT_THEME_VARS__: JSON.stringify(initialValue) // accsesble from react by __ANT_THEME_VARS__
        }
    }))
}else{
    console.log("Ant Theme active : false")
}

if(!prod){
    // add map file cheap-module-source-map,source-map,eval-source-map , source-map
    module.exports.devtool = "source-map";
}
// , new CopyWebpackPlugin([{from: './src/assets', to: './dist/assets'}])