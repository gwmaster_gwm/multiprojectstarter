# Description
```bash
build multiple projects that share same components container frameworks.
every project build independence. 
every project have separate json-server db and routes.
```

## Development

```bash
npm i 
npm run starterProject:server    // run JSON Server
npm run starterProject:start
```

## Build

```bash
npm run starterProject:build-dev  // have source map
npm run starterProject:build-pro
```

## Documentation

```bash
npm run docs  
npm run docs:min
```


## Dependencies


| Dependencies |  description |
|-----------------|---------------|
| [webpack 4](https://webpack.js.org/)| [webpack 4, is FAST (up to 98% faster)!](https://medium.com/webpack/webpack-4-released-today-6cdb994702d4)
| [ES6](http://es6-features.org/#Constants)/[7](https://medium.freecodecamp.org/ecmascript-2016-es7-features-86903c5cab70) | [ES7 Decorators in ReactJS](https://medium.com/@jihdeh/es7-decorators-in-reactjs-22f701a678cd) 
| [React  16.4](https://reactjs.org/) | [What’s new in React 16.3](https://www.infoworld.com/article/3228113/javascript/whats-new-in-react-163-javascript-ui-library.html)   , [New lifecycles ](https://reactjs.org/blog/2018/03/29/react-v-16-3.html)   ,   [Portal](https://hackernoon.com/using-a-react-16-portal-to-do-something-cool-2a2d627b0202)
|[React Router v4](https://github.com/ReactTraining/react-router) | standard routing library for React. When you need to navigate through a React application with multiple views
| [connected-react-router](https://github.com/supasate/connected-react-router) | Synchronize React Router v4 with redux store. Works for both redux-thunk and redux-saga.
| [REDUX](https://github.com/reactjs/redux) | Redux is a predictable state container for JavaScript apps 
| [react-redux](https://github.com/reactjs/react-redux) | Official React bindings for Redux
| [redux-saga](https://github.com/redux-saga/redux-saga) |  [API](https://redux-saga.js.org/docs/api/) middleware library, that is designed to make handling side effects in your redux app nice and simple
|  [redux-thunk](https://github.com/reduxjs/redux-thunk)   | middleware allows you to write action creators that return a function instead of an action.
| [redux-promise](https://github.com/redux-utilities/redux-promise) | The middleware returns a promise to the caller so that it can wait for the operation to finish before continuing
|  [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)   | chrome extension
| [SCSS](https://sass-lang.com/)  |  CSS with superpowers
| [LESS](https://sass-lang.com/)  |  It's CSS, with just a little more.
| [ESLint](https://eslint.org/) | ESLint is a tool for identifying and reporting on patterns found in ECMAScript/JavaScript code
| [React Hot Loader](http://gaearon.github.io/react-hot-loader/getstarted/) | React Hot Loader is a plugin that allows React components to be live reloaded without the loss of state



## UI


| Components |  description |
|-----------------|---------------|
| [@material-ui](https://material-ui.com/) | React components that implement Google's Material Design.
| [@material-ui/icons](https://material.io/tools/icons/?style=baseline) | Google's Material Icons.
| [semantic-ui-react](https://github.com/Semantic-Org/Semantic-UI-React) |Amazon ,Netflix's ,Microsoft's Teams prototyping
| [ant.design](https://ant.design/) | A design system with values of Nature and Determinacy for better user experience of enterprise applications
| [antd-theme-webpack-plugin](https://www.npmjs.com/package/antd-theme-webpack-plugin) | [example](https://antd-live-theme.firebaseapp.com/) change Ant Design specific color theme in browser
| [reset200802.css](https://meyerweb.com/eric/tools/css/reset/reset200802.css) | Reset basic html css './style/reset200802.css'


## Tools

| Framworks |  description |
|-----------------|---------------|
| [axios](https://github.com/axios/axios) | Promise based HTTP client for the browser and node.js
| [JSON Server](https://github.com/typicode/json-server) |  Get a full fake REST API with zero coding in less than 30 seconds
| [ESDoc](https://esdoc.org/) | Generates good documentation (plugin : ES6 , JSX)
| [react-redux-i18n](https://www.npmjs.com/package/react-redux-i18n/) | Internationalization and localization
| [redux-localstorage](https://github.com/elgerlambert/redux-localstorage) | The localStorage key used to store state
| [deepmerge](https://github.com/KyleAMathews/deepmerge) | Merges the enumerable attributes of two or more objects deeply.
| [Faker.js](https://github.com/Marak/faker.js) | generate massive amounts of fake data in the browser and node.js
| [lodash](https://lodash.com/) | A modern JavaScript utility library delivering modularity, performance & extras
| [react-keydown](https://www.npmjs.com/package/react-keydown) | Use react-keydown as a higher-order component or decorator to pass keydown events to the wrapped component, or call methods directly via designated keys
| [react-resize-detector](https://www.npmjs.com/package/react-resize-detector) | Nowadays browsers have started to support element resize handling natively using ResizeObservers. We use this feature (with a polyfill) to help you handle element resizes in React.
| [react-flexview](https://github.com/buildo/react-flexview/) | [API-Overview](http://buildo.github.io/react-flexview/API-Overview.html) - FlexView is first of all a React component, and like any (good) React component it’s stateless and works completely (and only) through props.
| [styled-css-grid](https://github.com/azz/styled-css-grid) | [examples](https://styled-css-grid.js.org/) [api](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout) Wrap your cells in Grid. Pretty simple.










Ivgeny Rafalovich - gwmaster.gwm@gmail.com

